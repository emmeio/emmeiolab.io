---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b><a href="https://docs.google.com/spreadsheets/d/1K7T26uEPrcAUBbjyo8R-BE8Rgu0rPc9FOl6Keyf8uK4/edit?usp=sharing" target="_blank">Livro de assinaturas / Guest book</a></b></p>
  <p><b>Produção e Curadoria:</b> Artur Cabral e Joenio Costa</p>
  
  <p><b>Assistentes de produção técnica e executiva:</b> Equipe MediaLab/UNB</p>
  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
      Alexandre Rangel <br>
      Amante da Heresia <br>
      Andrea Catropa <br>
      Domenico Barra (IT) 🇮🇹 <br>
      Flor de Fuego (AR) 🇦🇷 <br>
      Grupo Realidades <br>
      Guto Nóbrega <br>
      Hamilton Greene (USA) 🇺🇸 <br>
      Coletivo HOLOSCIUDADE <br>
      Ianni Luna <br>
      José Loures <br>
      Lorena Ferreira <br>
      Luisa Paraguai <br> 
      Lynn Carone <br>
      Marianne Teixido (MX) 🇲🇽 <br>
      Mark Al <br>
      Pedro Sagasti (AR) 🇦🇷 <br>
      Raisa Curty <br>
      Raquel Kogan <br>
      Rejane Cantoni <br>
      Rui Alão <br>
      Santiago Echeverry (USA) 🇺🇸 <br>
      Suzete Venturelli <br>
      Teófilo Augusto <br>
      Thatiane Mendes <br>
      Tiago Rubini <br>
      Zsolt & Bjørn (NL) 🇳🇱 <br>
    </span>
  </p>
</div>
</div>
</div>


