---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
        No envolvimento de uma perspectiva sensível com as novas possibilidade estéticas, oriundas das novas mídias, muitos artistas têm atuado e enfrentado os desafios propostos em um mundo contemporâneo. <br>
		Entendendo a arte como resultado de um entrelaçamento entre o universo instrumental, universo simbólico e o universo afetivo, os artistas têm revelado uma capacidade crescente de percepção, ação e transformação de estados estéticos nesse mundo contemporâneo, onde somos agentes e reagentes dentro de um emaranhado de forças e redes.<br>
 		É na multiplicidade de técnicas e poéticas dos trabalhos aqui apresentados, que reside a oportunidade de experienciar uma nova perspectiva artística, mediada na dimensão do virtual, a qual fomos forçados a conviver de forma ininterrupta durante essa pandemia.<br>
		Os trabalhos artísticos expostos nessa web-exposição buscam mediante uma minuciosa investigação poética, expandir nossa percepção dos fenômenos tecnológicos. Esse conjunto de obras trazem os afetos que surgem nas diferentes propostas poéticas, as quais passam pelas percepções de cada artista. <br>
		Nesse espaço virtual criam-se "janelas" para uma abertura mais ampla entre arte e as novas possibilidades de linguagem, as quais podem irradiar resultados dinâmicos, sensíveis e inesperados. São nessas janelas, ou melhor portas, que o público é convidado para entrelaçar-se em um mundo de interações, transformações poéticas e afetivas.
      </h5>
      <p class='nome-autor'>Artur Cabral , 2020</p>
    </div>
  </div>
</div>
