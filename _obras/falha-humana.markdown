---
layout: obra
title:  Falha Humana
thumbnail: /assets/tumbnail/t-falha.png
artista: Andrea Catropa
bio: http://www.andreacatropa.com/
video-descricao: 
texto-descricao: A escrita linear foi predominante para o pensamento moderno, associando-se, em grande parte, à reflexão crítica e à literatura. Em todo esse cadinho cultural, a busca pela expressão humana mais verdadeira dava o tom. Paulatinamente, a colaboração com as máquinas foi alterando essa aspiração e, além disso, trazendo para nós formas literárias que exploravam a simultaneidade e o imediatismo. A questão do tempo, da afetividade e da falibilidade de homens e de mulheres parece, cada vez mais, perder espaço para as utopias totalizantes do digital. Mas, mesmo que com outra face e com outras demandas, as máquinas de hoje ainda somos nós.
ano: 2020
---

<img src="/assets/falha-humana/lagrimas.gif" loop=infinite alt="Lagrimas" class="img-fluid">

`Lagrimas` 

<img src="/assets/falha-humana/luminosidade.gif" loop=infinite alt="A luminosidade dos pixels" class="img-fluid" width="850px">

`A luminosidade dos pixels` 

<video width="850" height="850" controls>
  <source src="/assets/falha-humana/logica.webm" type="video/webm"  />
  <source src="/assets/falha-humana/logica.mp4" type="video/mp4"  />
</video>

`A lógica do bot (coautoria com a Natasha Catrópa)` 
