---
layout: obra
title: Bombyx mori
thumbnail: /assets/tumbnail/t-bombyx.png
artista: Thatiane Mendes
bio: http://www.casulo.net/
video-descricao: 
texto-descricao: Peles são formadas por pontos e linhas, fios, camadas finas e espessas que cruzam e se entrecruzam sobre nosso corpo; várias e várias vezes. Estas linhas, camadas flexíveis e macias, são condutoras de muitas de nossas histórias íntimas, que por vezes se entrecruzam com as de outras pessoas, discursos amorosos, trajetos realizados, … O caminho que o bicho-da-seda (Bombyx mori) faz em volta do próprio corpo é um movimento repetitivo, a partir do qual ele obtém uma camada protetora para seu corpo, ziguezagueando sobre ele. Essa ação é como o movimento da costura e do tecer, ações transmitidas por muitas gerações de mulheres. Um movimento que também é repetitivo, que constrói camadas de superfícies, que nos  protegem e que amplia nossas capacidades sensoriais e perceptivas, mas que também nos moldam. É cada vez mais necessário identificar como as tecnologias fazem o controle sobre nossos corpos e nossas subjetividades; controle de identidades, dos modos de ser e viver.  Embora defenda-se que as tecnologias fazem um tipo de controle “sem gênero”, mais “suave”, realizado pelas grandes corporações, ainda temos nossas vidas subjugadas à condutas predefinidas por uma sociedade que não nos tem como protagonistas de nossas próprias vidas.
ano: 2020
---

<iframe src="https://player.vimeo.com/video/465578126" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<img src="/assets/bombyx/bombyx1.png" alt="Bombyx mori" width="832">
<img src="/assets/bombyx/bombyx2.png" alt="Bombyx mori" width="832">
<img src="/assets/bombyx/bombyx3.jpg" alt="Bombyx mori" width="832">


