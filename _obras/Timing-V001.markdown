---
layout: obra
title:  Timing V0.0.1
thumbnail: /assets/tumbnail/t-timing.png
artista: Teófilo Augusto
bio: https://www.teoaugusto.com.br/who-i-am
video-descricao: 
texto-descricao: Timing V 0.0.1 é uma obra computacional escrita em Processing com uma manifestação visual bem simples que visa investigar a relação da observação visual, a memória e a concepção cultural do Tempo como Dimensão. A Dimensão Tempo é uma dimensão construída pelo Ser Humano em uma clara ligação corporal com o Mundo, para "vermos" o Tempo nosso cérebro processa uma equiparação entre duas "imagens" que ele imediatamente rotula como antes ou depois, e é nessa comparação que se constrói a dimensão do tempo marcando que entre o antes e o depois deve ter acontecido algo. No Timing V 0.0.1 iniciam-se as "brincadeiras" com esse conceito, as imagens capturadas em tempos aleatórios vão sendo registradas em uma janela (MEMORY) enquanto que em outra janela (TIME) o objeto (por enquanto simbolizado por uma figura geométrica simples, no caso uma elipse) continua sua rotina indefinidamente.
ano: 2020
---

<video controls autoplay width="853" height="241">
  <source src="/assets/timing/timing.webm" type="video/webm"  />
  <source src="/assets/timing/timing.mp4" type="video/mp4"  />
</video>