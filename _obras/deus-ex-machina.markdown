---
layout: obra
title: Deus Ex Machina
thumbnail: /assets/tumbnail/t-deus.png
artista: Domenico Barra
bio: http://www.dombarra.art/biography
texto-descricao: How our experience with AI can enhance meanings and aspects of our existence that we take for granted? Deus Ex Machina takes out the dystopia from the narrative of the impact of AI in our society and proposes a counter-narrative where human life is enhanced by machines, human intelligence in symbiosis with artificial intelligence. Of course, we must be aware of the unexpected consequences, there will be glitches in this relationship, but those will not be the limit but an opportunity for improvement. It's always a matter of choices. We must praise but also nurture the symbiosis in healthy ways, it will grow and we will benefit from it. Reach awareness through imagination, new visions, new intelligence, new skin, new nature, new human, new life. Let yourself be inspired by Deus Ex Machina. <br> <br> Symbiosis - A good reason to work and cooperate with intelligent machines could be that of establishing connections, an intellectual approach, let's say on a philosophical ground, engaging in a symbiosis in a digital enviroment, enhancing our human nature, in close contact with machines through what we do. Nurture these "relationships" in a healthy way and let it grow. Other uses could just be toxic!
ano: 2020
---
<img src="/assets/deus/deus.gif" loop=infinite alt="Deus Ex Machina" class="img-fluid mx-auto d-block">

`Deus Ex Machina` 

<img src="/assets/deus/symbiosis.gif" loop=infinite alt="Symbiosis" class="img-fluid mx-auto d-block">

`Symbiosis` 


