---
layout: obra
title:  VRS - Vegetal Reality Shelter
thumbnail: /assets/tumbnail/t-vrs.png
artista: Guto Nóbrega
bio: https://www.gutonobrega.art/quem-somos
video-descricao: 
texto-descricao: Vegetal Reality Shelter é um sistema imersivo, criado com base em sons e imagens da natureza e na interação com plantas. Este trabalho é fruto de uma vivência na Floresta Amazônica organizada pelo LABVERDE, ocorrida durante os 10 dias do programa de residência artística na Reserva Florestal Adolpho Ducke. Trata-se de um pequeno domo imersivo (abrigo) com base na geometria de guarda-chuvas. Contém um pequeno sistema hidropônico com plantas, 6 canais de áudio e projetor de vídeo combinado a espelho esférico para projeção em domo. No interior do abrigo plantas são monitoradas quanto a resposta galvânica de suas folhas, que se altera segundo a respiração do visitante, quando este entra no espaço e interage com o sistema. Os dados monitorados nas plantas são utilizados para modificar a paisagem sonora e imagens da floresta em formato espelhado.
ano: 2019
---
<iframe src="https://player.vimeo.com/video/365129137" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


