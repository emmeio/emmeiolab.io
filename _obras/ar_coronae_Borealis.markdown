---
layout: obra
title: AR_Coronae_Borealis
thumbnail: /assets/tumbnail/t-ar.png
artista: Grupo Realidades (ECA – USP)
bio: http://www2.eca.usp.br/realidades/
video-descricao: 
texto-descricao: AR Coronae Borealis integra a série "quando as estrelas tocam", iniciada em 2019 pelo Grupo de Pesquisa Realidades (ECA USP). O conjunto de obras multimídia aborda a visualização de dados e a interpretação de informações sobre constelações e estrelas, particularmente as de magnitude variável, isto é, aquelas estrelas que apresentam variação de seu brilho ao longo dos anos. As informações e dados utilizados para realização da série estão disponibilizadas na base de dados da Associação Americana de Observadores de Estrelas Variáveis. Com base nessas medições, os autores propõem um percurso entre imagens e ondas&#58; a variação do brilho de uma estrela é compilada por um software que gera sons&#59; os sons modulam imagens que refletem luz sobre e para as estrelas.
ano: 2020
---
<img src="/assets/Coronae.jpg"  class="img-fluid" alt="AR_Coronae_Borealis">
<iframe src="https://player.vimeo.com/video/468620329" width="624" height="832" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>

<a href="https://www.instagram.com/ar/2736926153231374/">`Link para o trabalho no Instagram` </a> <br>
<a href="https://linktr.ee/ARCoronae">` Link para o projeto da série “Quando as Estrelas tocam”` </a>

>É necessário ter o aplicativo Instagram instalado em seu celular, na versão mais recente. É possível que o filtro utilizado não funcione adequadamente em todos os tipos de aparelho


<a href="http://www2.eca.usp.br/realidades/pt/ar-coronae-borealis/"> AR Coronae Borealis </a> integra a série “quando as estrelas tocam”, iniciada em 2019 pelo Grupo de Pesquisa Realidades (ECA/USP). O conjunto de obras multimídia aborda a visualização de dados e a interpretação de informações sobre constelações e estrelas, particularmente as de magnitude variável, isto é, aquelas estrelas que apresentam variação de seu brilho ao longo dos anos. As informações e dados utilizados para realização da série estão disponibilizadas na base de dados da Associação Americana de Observadores de Estrelas Variáveis (AAVSO – https://www.aavso.org). Com base nessas medições, os autores propõem um percurso entre imagens e ondas: a variação do brilho de uma estrela é compilada por um software que gera sons; os sons modulam imagens que refletem luz sobre e para as estrelas.

A constelação Coronae Borealis é descrita em mitologias antigas como a coroa oferecida como presente de matrimônio a Ariadne por Dionísio, com o qual a imortaliza. A estrela variável R Coronae Borealis, que compõe a constelação, foi ponto de partida para um processo poético e de pesquisa a distância do Grupo Realidades, por conta da situação de pandemia de 2020. Enquanto a primeira obra da série “quando as estrelas tocam”, R-Scuti de 2019, nos sugeria ouvir as imagens e suas mudanças no tempo, AR Coronae Borealis indica uma vontade de permanência: estar e sobreviver ao tempo.

Esta obra foi criada a partir da intervenção em Realidade Aumentada (AR), com acesso pela rede social Instagram. Os dados de magnitude da estrela são inseridos em outros programas para configurar padrões sonoros e visuais e realizar o design de interação. A obra artística se caracteriza também pela interação com redes sociais, possibilitando uma participação a distância e independente da necessidade de se instalar novos aplicativos, intercalando-se enquanto proposta poética com momentos cotidianos, naturais, das pessoas que já estão imersas nessas redes. Portanto, usou-se o software Spark AR Studio, para desenvolver um filtro que se integra a rede social Instagram e permitir, dessa forma, interação dos usuários que a usam cotidianamente.

<i>Autoria:</i> Grupo Realidades (ECA – USP in http://www2.eca.usp.br/realidades/). Bruna Mayer, Clayton Policarpo, Dario Vargas, Karol Pinto, Letícia Brasil, Loren Bergantini, Marcus Bastos, Miguel Alonso, Paula Perissinotto, Sergio Venancio, Silvia Laurentiz - (astrônomo colaborador: Rodrigo G. Vieira).
