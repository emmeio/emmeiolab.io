---
layout: obra
title: 0rfr1d[A]b0rg a Ext3rminAd0rA do prEs3ntE diStOpic0
thumbnail: /assets/tumbnail/t-0rfr1d.png
artista: AmAntE dA hErEsiA
bio: https://amantedaheresia.blogspot.com/
video-descricao: 
texto-descricao: Vídeo-remix de anarquismo fantástico e pirataria libertária "0rfr1d[A]b0rg - a Ext3rm1n[A]d0r4 do pr3$EntE d14tÓp1c0". Orfridaborg - a exterminadora do presente distópicorealizado por AmAntE dA hErEsiA, inspirado no trailer do filme "O exterminador do Futuro"(the terminator, 1984). a presente obra é um trailer acerca da realidade que em breve viveremos. "baseada em fatos reais" sua narrativa é construída a partir da colagem de trechos de vídeos retirados da internet, composição própria de trilha sonora e inserções de artes gráficas.seu tema se dá como um futuro utópico evocado desde nosso presente distópico para que a utopia futura resolva o grande problema da desertificação do cerrado, a distopia presente. as personagem dessa trama são as plantas ciborgues do presente F-ORCHIS e di0lÉiA dUck vAdEr, e a planta ciborgue do futuro 0rfr1dAb0rg contra os agentes da desertificação - jagunços teocráticos, capitães do mato universitários, bandeirantes 2.0, neoGrileiros, acionista-bombas e drones do agropop. <br> <b>[A]m[A]nt[E] d[A] h[E]r[E]si[A] {lÉ0 p1m3nt3l}</b><br>artista multimídia por forças distorsivas desobedientes & em rebeldia. Punk, negríndio, anarquista, não-ocidental, hacker, cigano, filósofo, herege, queer, artificialista.. Obra dispersa pelos ruídos de minha antimúsica (pUnk[A]l_sUlUk & rUÍd0s em rUÍnAs) + pelo jogo repetitivo de símbolos que obsessivamente aparecem em minhas colagens (cYb10s3 de uma ArtEsAniA d1g1t4l) + pela poluição de um caos ordenado nada surreal em meus vídeos (cIn’sUrg[E]nt[E]) + & pela constante destruição das regras gramaticais em meus textos (cEmitÉr10 d3 [E]l[E]pUnks)
ano: 2020
---

<iframe src="https://player.vimeo.com/video/465178305" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>