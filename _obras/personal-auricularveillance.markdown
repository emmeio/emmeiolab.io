---
layout: obra
title: Personal Auricularveillance
thumbnail: /assets/tumbnail/t-auricular.png
artista: Lorena Ferreira
bio: https://lorenaferreira.gitlab.io/bio.html
video-descricao: 
texto-descricao: Casos de recebimento de anúncios de produtos foram testemunhados após a fala de alguma necessidade em voz audível próximo ao aparelho celular. Teoria da conspiração? Há estudos que denunciam a vigilância sonora praticada por empresas de comunicação e tecnologia. A boa notícia é que você não precisa mais se preocupar! O Personal Auricularveillance foi desenvolvido para que você possa burlar essa vigilância! Através de uma prótese de orelha humana acoplada ao microfone do seu aparelho celular, você (o vigiado) poderá sabotar ou ajudar o seu vigilante, alterando, bloqueando, ou mesmo intensificando o conteúdo sonoro monitorado de acordo com suas necessidades.
ano: 2020
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/auricular/personal2.jpg" alt="Personal Auricularveillance" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal.jpg" alt="Personal Auricularveillance" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal3.jpg" alt="Personal Auricularveillance" class="img-fluid mx-auto d-block">>
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal4.jpg" alt="Personal Auricularveillance" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal6.jpg" alt="Personal Auricularveillance" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal8.jpg" alt="Personal Auricularveillance" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/auricular/personal9.jpg" alt="Personal Auricularveillance" class="img-fluid">
      </div>
    </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>


<div class="d-block d-sm-none">
  <img src="/assets/auricular/personal2.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal3.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal4.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal6.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal8.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal9.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%"><br><br>
  <img src="/assets/auricular/personal.jpg" alt="Personal Auricularveillance" class="img-fluid" width="35%">

</div>


