---
layout: obra
title: Entre Margaridas
thumbnail: /assets/tumbnail/t-margaridas.png
artista: Luisa Paraguai
bio: https://www.luisaparaguai.art.br/
video-descricao: 
texto-descricao: A obra interativa  “e n t r e M a r g a r i d a s ” pretende operacionalizar a representatividade feminina pela cartografia da família das margaridas (Asteraceae), enquanto uma flora endêmica comum do Brasil, que ao se desenvolver em lugares inesperados e se adaptar em diversos solos, evoca a mesma força e resistência construídas pelas muitas Margaridas brasileiras, como Teresa Margarida Silva e Orta e Margarida Maria Alves, também geolocalizadas. <br> Assim, a parBr da estrutura do Google Maps serão mapeadas no Brasil espécies da família Asteraceae em extinção. <br> E em composição com estas informações, estórias pessoais serão situadas a partir da colaboração dos usuários, enviadas por email [breve história de vida, fotografia, localização]. Instala-se uma cartografia afetiva, que pretende mapear e divulgar o contexto destas Margaridas ameaçadas em suas dimensões botânicas ou políticas. A obra “entre Margaridas” resgata um arquivo científico para organizar uma coleção afetiva, que atualiza um olhar urgente sobre as espécies em perigo – um território ambiental e histórico-cultural que vem sendo gradativamente destruído, para aproximar de uma outra realidade socio-política, as nossas Margaridas, que também sobrevivem.
ano: 2020
---

<iframe src="https://www.google.com/maps/d/embed?mid=1mJOW4C_pdTfpiRg-280JhzyLPVRN2wQ9" width="832" height="624"></iframe>
