---
layout: obra
title:  Pi(x)el
thumbnail: /assets/tumbnail/t-pixel.png
artista: Flor de Fuego e Pedro Sagasti
bio: https://flordefuego.github.io/
video-descricao: 
texto-descricao: I touch my screen and reach for  people. I look in between words for some interaction, trying to feel close, not alone. Wanting this to be real. The nearness in loneliness. I touch. Drawing with my fingers some impulse to say what I am feeling. Do screens feel something? Are my emotions data? I have nothing but my bytes. This is a documentation of a work in progress. An encounter and construction of artistic processes through distance, in isolation. An exchange from a work in progress given between two artists, Florencia Alonso (Buenos Aires) and Pedro Iñaki Sagasti (La Plata). The spaces in the story were built through virtual exchanges which were assembled in a progressive way. Interconnecting videos builted through programming, text, reflections and sound. Searching for generating acoustic spaces, virtual depth, delving in the intersect between languages; creating different kinds of spatialities.
ano: 2020
---
<iframe src="https://player.vimeo.com/video/467547809" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p>
<b>Flor de Fuego</b> <a href="https://flordefuego.github.io/"  target="_blank"> flordefuego.github.io </a>
<b>Pedro Sagasti</b> <a href="https://torotumbo.bandcamp.com/"  target="_blank"> torotumbo.bandcamp.com </a>
</p>


