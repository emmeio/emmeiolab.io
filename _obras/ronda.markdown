---
layout: obra
title:  Ronda
thumbnail: /assets/tumbnail/t-ronda.png
artista: Lynn Carone
bio: https://lynncarone.com/home/
video-descricao: 
texto-descricao: Quando se constrói um ninho, questões como o que é interno ou externo, objetivo ou subjetivo, real ou virtual, da ordem da matéria ou ser, tornam-se entrelaçamentos em um ambiente onírico e afetivo. A água que é palco da morte no mito de Ofélia, torna-se um convite à celebração da vida. Eis que podemos finalmente inventar um jeito efetivo de sonhar acordado, quando se grava aquilo que se sonha. Girar sobre si, pode ser um chamado para olhar para dentro.
ano: 2012
---
<iframe src="https://player.vimeo.com/video/466737616" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


