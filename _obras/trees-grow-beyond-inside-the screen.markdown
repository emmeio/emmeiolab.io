---
layout: obra
title:  "Trees grow beyond-inside the screen"
thumbnail: /assets/tumbnail/t-trees.png
artista: Suzete Venturelli
bio: https://labfront.weebly.com/
video-descricao: 
texto-descricao: Essa proposta artística toma como poética a relação entre arte computacional, arte generativa e natureza, para entender o que somos e como pensar no mundo de hoje Ela considera que reduzir o desmatamento não é suficiente para o nosso planeta. A poética da proposta artística aponta para a necessidade de reflorestar áreas afetadas pelo desmatamento. Nesse sentido, apropriamos de informações sobre o reflorestamento da Mata Atlântica, para oferecer uma metáfora da possibilidade de cultivar nova vegetação como um novo mundo, no desenvolvimento da vida na Terra.
ano: 2020
---
<iframe class="frame" src="/obras-externas/trees-grow-beyond-inside-the-screen/"></iframe>
