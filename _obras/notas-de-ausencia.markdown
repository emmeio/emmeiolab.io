---
layout: obra
title: Notas de ausencia
thumbnail: /assets/tumbnail/t-notas.png
artista: Marianne Teixido
bio: https://marianneteixido.github.io/
video-descricao: 
texto-descricao: Notas de ausencia it’s a generative web essay, passing data to a computer as in a resignifying agent, deconstructing discursive structures that resemantics the narratives about the disappearing of woman in Mexico and Latin America. Both virtual space and time makes up a sheet for the recall and denunciation, the narrative, semi autonomous, argues from feminist tweets, poetry, books and articles that explain from a theoric perspective, forced dissapearing, feminicide and gender violence. <br> Shown up as texts, images and sounds. The narrative of the art piece it’s assembled between two bots. the first one, that shares tweets and finds with hashtags like &#58; #MéxicoFeminicida, #MadresEnBúsqueda, #ViolenciadeGenero, #NiUnaMenos, between others, also sharing tweets made by an automatic text generation bot. This sonorous-textual composition integrates as a procedual essay, in a manner of net-art, denouncing the violence against women in real time during the pandemic time.
ano: 2020
---
<iframe class="frame" src="https://notasdeausencia.cc/memorial/"></iframe>


<h4>Credits: </h4>
<b>Design, music, poetry and academic extracts selections:</b> Marianne Teixido.

<b>Twitter bot and markov bot development:</b> Dorian Velázquez. 

<b>Data processing and web development:</b> Marianne Teixido, Dorian Velázquez, Emilio Ocelotl.
