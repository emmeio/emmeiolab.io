---
layout: obra
title: Bicho BR060
thumbnail: /assets/tumbnail/t-bicho.png
artista: Raisa Curty
bio: https://www.instagram.com/raisa.curty/
video-descricao: 
texto-descricao: BICHOS BR060 (2017), são compostos por ossos de cachorros atropelados e sistemas eletrônicos de brinquedos. Eles caminham de forma desgovernada dentro de um cercado de 90x90cm. Um deles rasteja, o outro se move de forma nervosa. <br> Este trabalho foi realizado no contexto do Salão/Residência Artística Eixo do Fora (DF), para onde enviei uma proposta de ação que consistia em caminhar os 90 quilômetros do trecho que liga Brasília (DF) a Olhos D'água (GO), local onde a residência artística foi realizada. Ao longo do percurso me propus a coletar ossos de animais vítimas das rodovias e tratá-los durante a residência que aconteceria no Núcleo de Arte do Centro Oeste - NACO (GO). Encontrei ossos de cachorros, sapos, lagartos, tatus, cobras e galinhas.  <br> Durante o período de residência convivi com os ossos em uma cozinha onde realizava os procedimentos de limpeza e organização do material. Juntei os ossos a esqueletos de brinquedos eletrônicos&#58; Um boneco do capitão américa que rastejava pelo chão e disparava tiros com sua metralhadora e um pequeno cão raivoso que latia ao vento. A performance de objeto, BICHOS BR060, aconteceu durante todo o mês de novembro de 2017 no Museu Nacional da República (DF).
ano: 2017
---
<iframe src="https://player.vimeo.com/video/465465657" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/465465983" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>