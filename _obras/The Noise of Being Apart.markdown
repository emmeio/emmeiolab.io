---
layout: obra
title:  The Noise of Being Apart
thumbnail: /assets/tumbnail/t-noise.png
artista: Ianni Luna
bio: https://ianniluna.net/
video-descricao: 
texto-descricao: As sonoridades de uma dança com o desconhecido incerto em contextos de pandemia global. Ianni Luna é doutoranda em Arte e Tecnologia pela UnB, tendo desenvolvido sua pesquisa em arte sonora. Atualmente vive e trabalha em Berlim. Seus interesses têm se voltado para as relações entre arte e ficção, desde uma investigação por poéticas relacionais.
ano: 2020
---

<iframe src="https://player.vimeo.com/video/465182399" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>