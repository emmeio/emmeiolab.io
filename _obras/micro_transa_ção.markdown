---
layout: obra
title:  Micro_transa_ção
thumbnail: /assets/tumbnail/t-micro.png
artista: José Loures
bio: http://instagram.com/loures_jose
video-descricao: 
texto-descricao: O trabalho “Micro_transa_ção” se debruça sobre o meu corpo sendo retratado durante 72 dias entre 21 de março a 31 de maio de 2020. Tenho o ritual de desenvolver autorretratos como uma magia de autocura, em um momento de reflexão sobre minhas escolhas e consequências diante a vida. Um período que desfoco o mundo a minha volta, e retorno para mim. Os 72 desenhos foram trabalhados a partir da dualidade e polaridade emocional representada na convergência entre o preto e o branco. Nesse contexto, a atual condição de isolamento social força o olhar para dentro, não há mais festas, nem shows, nem bares para nos anestesiarmos da condição de existir. Não há como fugir e esse enfrentamento é inevitável, em um momento de se compreender e lidar com seus demônios interiores. As imagens apresentam uma trilha labiríntica sobre o meu corpo enclausurado pela quarentena e que agora somente existe via os limites dos smartphones. As polaridades de um processo criativo que evoca começos e fins, afeto, insegurança, sexualidade, dor, saudade, fome seja na solitude ou nos relacionamentos. O meu corpo dentro de uma caixa que também está dentro de outra caixa perante as esferas espirituais, artísticas, poéticas e carnais. <br> Os desenhos foram concebidos por meio de uma técnica híbrida&#58; fotografia (selfies), desenho (papel A3 e nanquim), escâner sujo, finalização em softwares de edição de imagem (Photoshop) e posteriormente no formato de vídeo (Sony Vegas). O áudio presente no trabalho são comentários postados sobre os desenhos em meu Instagram. 
ano: 2020
---


<script>
  // alert();
  if (confirm("ATENÇÃO! \n O conteúdo deste projeto é desaconselhável para menores de 18 anos. As imagens que você verá a seguir são destinadas apenas para pessoas maiores de idade. Se você for maior de 18 clique em OK.")) {
} else {
  window.location.replace("/");
}

</script>
<div class="menu d-none d-sm-block">
	<div style="width: 100%; height: 100%; overflow: hidden">
	<iframe class="frame" src="https://jloures-arte.wixsite.com/72toques" scrolling="no" style="position: relative; left: -80px ;top: -110px; height: 794px"></iframe>
	</div>
</div>


<div class="d-block d-sm-none">
	<div style="width: 100%; height: 100%; overflow: hidden">
	<iframe class="frame" src="https://jloures-arte.wixsite.com/72toques" scrolling="no" style="position: relative; left: 0px ;top: -50px; height: 580px"></iframe>
	</div>
</div>

<!--  style="position: relative;top: -100px" -->