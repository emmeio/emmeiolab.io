---
layout: obra
title:  ARBATAX | Design procedural 
thumbnail: /assets/tumbnail/t-arbatax.png
artista: Rui Alão
bio: https://www.caramboladigital.com.br/
video-descricao: 
texto-descricao: Em meados de 2019 recebemos uma solicitação de projeto por parte da Arbatax Arqutietura, do Arq. Fernando Laterza, para projetar a identidade visual da empresa, que tem especialidade em projetos com inspiração biomimética, ou seja, inspiração em estruturas da natureza, principalmente em organismos vivos. Ao invés de usar a estratégia tradicional de criar algo projetado e definido, queria que a identidade tivesse algumas características das coisas vivas, isto é, que evoluísse e que fosse um pouco diferente a cada vez. Usei um código p5.js e fiz três versões da identidade, com triângulos, círculos e quadrados.
ano: 2020
---
<div style="width: 100%; height: 100%; overflow: hidden">
<iframe class="frame" src="https://editor.p5js.org/ruialao/embed/9zFR48Ymm" scrolling="no" style="height: 100%;"></iframe>
<iframe class="frame" src="https://editor.p5js.org/ruialao/embed/XhHfErLYB" scrolling="no" style="height: 100%;"></iframe>
<iframe class="frame" src="https://editor.p5js.org/ruialao/embed/l144VrUMZ" scrolling="no" style="height: 100%;"></iframe>
</div>
