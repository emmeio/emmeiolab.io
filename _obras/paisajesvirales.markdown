---
layout: obra
title:  Paisajes Virales
thumbnail: /assets/tumbnail/t-paisajes.png
artista: HOLOSCIUDADE
bio: http://www.espai214.org/holos/
video-descricao: 
texto-descricao: O projeto Paisaje Virales, desenvolvido pelo coletivo HOLOSCIUDADE, com a coordenação de criação de Marcos Umpièrrez e coordenação curatorial de Lilian Amaral é um projeto interativo, apresentando 5 experiências sonorovisuais&#58; Exp. 1 – Tapabocas, Liliana Fracasso e Francisco Cabanzo; Exp. 2 –  Unosiotrosno | Francisco Cabanzo,&#59; Exp. 3 – Respira | Lilian Amaral e Laurita Salles&#59; Exp. 4 – LineaVida | Bia Santos e Emílio Martinez&#59; Exp. 5 – Normal | Josep Cerdá. <br>Proposta aberta y processual de cocriação sonorovisual, o trabalho é composto por registros sonoros coletados em diversas cidades ibero-americanas e objetos visuais com complementam e potencializam a sonoridade e as interações. Há, nesses objetos, visuais, certos tipos de resistências, associadas desde o íntimo, o introspectivo, o público, coletivo e o compartilhado. Cinco experiências sonorovisuais que nos convidam a deixar os lugares de confinamento.<br>Paisajes Virales, 2020 é uma criação coelaborativa desenvolvida por membros do coletivo HolosCi(u)dad(e) –  http://www.espai214.org/holos/.  Grupo de pesquisa-criação da rede ibero-americana no campo da arte, ciência e tecnologia. Participam desse trabalho artistas pesquisadores do Uruguai, UDELAR, Colômbia – UAN, Brasil – UFG, UFRN e UnB e Espanha – UB e UPV / ANIAV.<br>Co-elaboram&#58; Marcos Umpièrrez, Lilian Amaral, Laurita Salles, Suzete Venturelli, Liliana Fracasso, Francisco Cabanzo, Bia Santos, Emílio Martinez e Josep Cerdá.
ano: 2020
---
<img src="/assets/PaisajesVirales.png"  class="img-fluid" alt="Paisajes Virales" width="832">
<br><br>

<a href="https://paisajesvirales.sonoro.space/" class="link-round" target="_blank">CLIQUE PARA ACESSAR O TRABALHO</a>
<br><br>
<p>
O projeto Paisaje Virales, desenvolvido pelo coletivo HOLOSCIUDADE, com a coordenação de criação de Marcos Umpièrrez e coordenação curatorial de Lilian Amaral é um projeto interativo, apresentando 5 experiências sonorovisuais&#58; Exp. 1 – Tapabocas, Liliana Fracasso e Francisco Cabanzo; Exp. 2 –  Unosiotrosno | Francisco Cabanzo,&#59; Exp. 3 – Respira | Lilian Amaral e Laurita Salles&#59; Exp. 4 – LineaVida | Bia Santos e Emílio 
Martinez&#59; Exp. 5 – Normal | Josep Cerdá.
<br>
<i>Co-elaboram&#58;</i> Marcos Umpièrrez, Lilian Amaral, Laurita Salles, Suzete Venturelli, Liliana Fracasso, Francisco Cabanzo, Bia Santos, Emílio Martinez e Josep Cerdá.
</p>