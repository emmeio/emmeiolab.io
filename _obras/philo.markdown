---
layout: obra
title:  PHILOBIO+METRIC
thumbnail: /assets/tumbnail/t-philo.png
artista: Mark Al
bio: http://marckal.com/about/
video-descricao: 
texto-descricao: Biometry uses statistical methods to analyze and store data about ourselves and our knowledge. This short movie is about this relationship.
ano: 2017
---
<iframe src="https://player.vimeo.com/video/244099843" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


