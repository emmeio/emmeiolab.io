---
layout: obra
title: BOGUE
thumbnail: /assets/tumbnail/t-bogue.png
artista: Santiago Echeverry
bio: http://santi.tv/hot
video-descricao: 
texto-descricao: The House of Tupamaras is a group of Voguing performers based in Bogota, Colombia. They are professionally trained dancers that chose to break from the norm and explore the queerness of their bodies and their everyday self-expressions to turn their art form into a political tool, questioning the double standards of Colombian society. They create impromptu happenings, choreographed dances, kiki balls, etc., and have performed on stage with renowned groups such as Pussy Riot, among others. This series of prints and videos seeks to capture the Tupamaras' attitude, athletic musculature, and spunky personalities. The series of volumetric portraits, that isolate spatially the dancers and create a baroque-like atmosphere, are captured in real time with the Kinect sensor, using custom Java code in Processing 3.0, written by the artist. Photoshop is used for color correction and sizing purposes, without any additional filters. Each individual image, captured in HD at 12 to 16 frames per second, composes longer animated still frame sequences, that are consequently edited into the experimental video component of the series, BOGUE. Seeing Voguing as a dance duel, the original music mixes the sounds of two very famous scenes of Latino soap operas, that portray exaggerated - but absurd - female fights.
ano: 2020
---
<iframe src="https://player.vimeo.com/video/368369095" width="832" height="624" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

<i>World Premiere:</i> Connect Videoart Festival Tampa, FL, 10/23/2019<br>
<i>Year:</i> 2019 <br>
<i>Duration:</i> 3'30" <br>
<i>Performance and Choreography:</i> The House of Tupamaras @houseoftupamaras <br>
<i>Music:</i> Patty E. Patétik<br>
<i>Producer:</i> Jaime H. Arboleda R. @restrepo_beto<br>
<i>Thanks:</i> Corporación BDSM Colombia @corporacionbd.sm<br>
<i>Production and shooting location:</i> Bogota, Colombia<br>
<br>
<i>Performers:</i><br>
Baretamara @da_azuladx<br>
Cobra Tamara @me.alejo<br>
Honey Vergony @honeyvergony<br>
Lady Hunter @ladyhunter__<br>
Laika Tamara @laikatamarahot<br>
Jona Tamara @lxjona_tamara<br>
Pvssy Divx @pvssydivx<br>
Roxy Tamara @andres.nash
