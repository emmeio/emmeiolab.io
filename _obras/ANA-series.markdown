---
layout: obra
title: ANA series
thumbnail: /assets/tumbnail/t-ana.png
artista: Hamilton Greene
bio: https://hamy.xyz/about/
video-descricao: 
texto-descricao: Ana (Artifice-native art) is an exploration of the capabilities of artificial intelligence and human perceptions of art and beauty. The works are created through various AI technologies and presented as stand-alone artifacts representing this intersection of humanity and technology.
ano: 2020
---


<img src="/assets/ana/seed1.png" loop=infinite alt="Deus Ex Machina" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/ana/seed2.png" loop=infinite alt="Deus Ex Machina" class="img-fluid mx-auto d-block">
<br>
<img src="/assets/ana/seed3.png" loop=infinite alt="Deus Ex Machina" class="img-fluid mx-auto d-block">
