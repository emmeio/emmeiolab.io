---
title: Instruções para transmissão (streaming)
layout: default
---

## Instruções para transmissão (streaming)

* Os slots são de 20 minutos no total, cada performance deve começar e terminar no horário.
* Tente iniciar a transmissão no horário correto.
* Inicie sua transmissão idealmente alguns segundos antes do seu horário. Se você entrar antes pode cortar a transmissão anterior.
* Se possível adicione informações no vídeo incluindo seu nome, local, etc
* Recomendamos o uso do Open Broadcaster Software (OBS), software livre disponível para Linux, Mac e Windows em [https://obsproject.com](https://obsproject.com)

#### Configuração
Abra as configurações do OBS em "File" > "Settings". Lá acesse as seguintes abas e defina os parâmetros da transmissão:

- Na aba "Stream":
	* Service: Youtube

- Na aba "Output":
   * Video bitrate: 500 
   * Audio bitrate: 128

- Na aba "Video":
   * FPS: 10
   * Output (Scaled) resolution: 854x480.
   
Estes valores de configuração definem uma transmissão de qualidade relativamente baixa,
porém mais segura para evitar problemas de conexão.
Caso você tenha uma boa conexão pode usar valores de bitrate, fps ou resolução maiores.

Estas são apenas sugestões gerais, você pode preferir definir seus próprios parâmetros.
   
#### Teste sua transmissão
1. Na aba "Stream", entre a Stream Key `7x9v-gu22-uw4c-ev29-92s8`
2. Inicie a transmissão em "Start Streaming"
3. Verifique se está ao vivo em [https://youtu.be/rgTGrMo7Zqs](https://youtu.be/rgTGrMo7Zqs)

Uma boa fonte para ajuda caso algo dê errado no teste é essa aqui:
* [https://support.google.com/youtube/answer/2853702?hl=en-GB](https://support.google.com/youtube/answer/2853702?hl=en-GB)

#### Na hora da performance
1. Na aba "Stream", entre a Stream Key fornecida por e-mail.
2. Aguarde até alguns segundos antes sua hora agendada na [página de performances do evento](/performances).
3. Inicie sua transmissão em "Start Streaming"
4. A transmissão estará ao vivo no [canal do Media/UnB](https://www.youtube.com/playlist?list=PLxkIqaMJsWqUY0zIjqVVt2kD4KPeWiynq)
5. Prepare-se para terminar a transmissão alguns segundos antes do fim do seu horário.


--

documento baseado nas instruções do eulerroom equinox, disponível em:
https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew
